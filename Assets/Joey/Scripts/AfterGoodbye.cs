using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterGoodbye : MonoBehaviour
{
    public Color backgroundColor;
    public GameObject environment;


    public void SayGoodbye()
    {
        // turn off all objects
        dance[] characters = GameObject.FindObjectsOfType<dance>();
        foreach(var obj in characters) {
            Destroy(obj.gameObject);
        }
        // destory environment
        Destroy(environment);

        // go to black
        Camera.main.clearFlags = CameraClearFlags.SolidColor;
        Camera.main.backgroundColor = backgroundColor;
    }
}
