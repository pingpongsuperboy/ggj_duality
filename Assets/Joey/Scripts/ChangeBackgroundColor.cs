using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBackgroundColor : MonoBehaviour
{
    public Color newBackgroundColor;


    public void ApplyNewColor()
    {
        Camera.main.backgroundColor = newBackgroundColor;
    }
}
