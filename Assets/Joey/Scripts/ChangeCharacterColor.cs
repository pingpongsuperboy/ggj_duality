using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCharacterColor : MonoBehaviour
{
    public Color color;
    public Renderer[] characterRenderers;


    public void Apply()
    {
        foreach(var r in characterRenderers) {
            r.material.color = color;
        }
    }
}
