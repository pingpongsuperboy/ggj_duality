using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeQuestionImageColor : MonoBehaviour
{
    public Color newColor;


    public void Apply()
    {
        Image image = GetComponent<Image>();
        image.color = newColor;
    }
}
