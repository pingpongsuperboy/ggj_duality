using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventOnKey : MonoBehaviour
{
    public KeyCode key;
    public UnityEvent events;


    void Update()
    {
        if(Input.GetKeyDown(key)) {
            events.Invoke();
        }
    }
}
