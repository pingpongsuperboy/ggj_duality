using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeLots : MonoBehaviour
{
    public Transform[] placesToPutCopies;
    public Transform[] placeToPutCopiesWhenPenguin;
    public GameObject characterCreator;

    List<GameObject> copies;


    void Start()
    {
        copies = new List<GameObject>();
    }


    public void Apply()
    {
        copies.Clear();
        Transform[] locations = IsPenguin()? placeToPutCopiesWhenPenguin : placesToPutCopies;
        
        foreach(var position in locations) {
            var copy = GameObject.Instantiate(characterCreator, position.position, position.rotation);
            copy.transform.localScale = position.localScale;
            copies.Add(copy);
        }
    }


    public void TrackChange()
    {
        // if no copies...
        if(copies.Count == 0) {
            // do nothing
        }
        // else delete them and recreate
        else {
            foreach(var copy in copies) {
                Destroy(copy);
            }

            Apply();
        }
    }


    bool IsPenguin()
    {
        return CharacterFlagsTracker.instance.HasAttribute(CharacterFlagsTracker.Attribute.Penguin);
    }
}
