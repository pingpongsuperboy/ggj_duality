using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartPenguinActivator : MonoBehaviour
{
    public GameObject answer1;
    public GameObject answer2;
    public GameObject answer1Penguin;
    public GameObject answer2Penguin;


    bool isActivated;


    void Start()
    {
        isActivated = false;
    }


    public void ActivateAnswer1()
    {
        if(IsPenguin()) {
            Debug.Log("is penguin!");
            if(answer1Penguin != null)
                answer1Penguin.SetActive(true);
        }
        else {
            Debug.Log("not penguin!");
            answer1.SetActive(true);
        }

        isActivated = true;
    }


    public void ActivateAnswer2()
    {
        if(IsPenguin()) {
            Debug.Log("is penguin!");
            if(answer2Penguin != null)
                answer2Penguin.SetActive(true);
        }
        else {
            Debug.Log("not penguin!");
            answer2.SetActive(true);
        }

        isActivated = true;
    }


    public void OnBecomePenguin()
    {
        // if we've already shown stuff, move to the penguin stuff
        if(isActivated) {
            // if we answered answer1, switch to penguin
            if(answer1.activeInHierarchy) {
                answer1.SetActive(false);
                if(answer1Penguin != null)
                    answer1Penguin.SetActive(true);
            }
            else if(answer2.activeInHierarchy) {
                answer2.SetActive(false);

                if(answer2Penguin != null)
                    answer2Penguin.SetActive(true);
            }
            else {
                Debug.LogError("something went wrong with smart penguin...");
            }
        }
        else {
            Debug.Log($"{gameObject.name} hasn't been activated yet!!");
        }
    }


    bool IsPenguin()
    {
        return CharacterFlagsTracker.instance.HasAttribute(CharacterFlagsTracker.Attribute.Penguin);
    }
}
