using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class QuestionsController : MonoBehaviour
{
    public enum QuestionType { Appearance = 0, Personality = 1, Environment = 2, Story = 3, Ending = 4, Complex = 99 }

    [System.Serializable]
    public class QuestionAndAnswer
    {
        public string question;
        public string answer1;
        public string answer2;
        
        [Tooltip("If the player chooses Answer 1, then we will invoke this event.")]
        public UnityEvent answer1Event;
        [Tooltip("If the player chooses Answer 2, then we will invoke this event.")]
        public UnityEvent answer2Event;

        public bool answer1TrackChange = false;
        public bool answer2TrackChange = false;
        public CharacterFlagsTracker.Attribute answer1Attribute;
        public CharacterFlagsTracker.Attribute answer2Attribute = CharacterFlagsTracker.Attribute.NA;

        [System.NonSerialized]
        public bool hasBeenShown;
    }

    public QuestionAndAnswer[] appearanceQuestions;
    public QuestionAndAnswer[] personalityQuestions;
    public QuestionAndAnswer[] environmentQuestions;
    public QuestionAndAnswer[] storyQuestions;
    public QuestionAndAnswer[] complexQuestions;
    public QuestionAndAnswer[] endingQuestions;

    public int numAppearanceQuestions = 10;
    public int numPersonalityQuestions = 8;
    public int numEnvironmentQuestions = 4;
    public int numStoryAndComplexQuestions = 8;

    public Text questionText;
    public Text answer1Text;
    public Text answer2Text;
    public float waitTimeBetweenQuestions = 1.0f;
    public QuestionType firstQuestionType = QuestionType.Appearance;
    public int firstQuestionIndex;
    public MakeLots makeLotsScript;
    public int musicQuestionIndex;

    QuestionType currentQuestionType;
    QuestionAndAnswer currentQuestion;
    int questionCounter;
    bool justAskedStoryQuestion;
    int endingQuestionCounter;
    bool hasShownMusicQuestion;


    // how many questions we ask before asking the player if they'd like to move on
    const int NUM_QUESTIONS_BEFORE_ASKING_TO_MOVE_ON = 5;
    // text to ask the player if they want to move to the next question type
    const string MOVE_ON_QUESTION_TEXT = "Are you happy with what they look like?";
    // used to track if we're currently asking the player to move on
    QuestionAndAnswer moveOnQuestion;


    void Start()
    {        
        // init questions
        InitQuestions();

        // did they ask to show a specific question at the start?
        if(firstQuestionIndex >= 0) {
            currentQuestionType = firstQuestionType;
            QuestionAndAnswer question = new QuestionAndAnswer();

            // pull question from the right pool
            switch(firstQuestionType) {
                case QuestionType.Appearance:
                    question = appearanceQuestions[firstQuestionIndex];
                    break;
                case QuestionType.Personality:
                    question = appearanceQuestions[firstQuestionIndex];
                    break;
                case QuestionType.Environment:
                    question = appearanceQuestions[firstQuestionIndex];
                    break;
                case QuestionType.Story:
                    question = appearanceQuestions[firstQuestionIndex];
                    break;
            }

            // set texts to chosen question
            currentQuestion = question;
            questionText.text = question.question;
            answer1Text.text  = question.answer1;
            answer2Text.text  = question.answer2;

            // mark question as seen
            MarkQuestionAsShown(currentQuestion.question, currentQuestionType);
        }
        // if not, just show random appearance one
        else {
            currentQuestionType = QuestionType.Appearance;
            ShowNextQuestion();
        }      

        justAskedStoryQuestion = false;  
        endingQuestionCounter = 0;
        hasShownMusicQuestion = false;
        
        Debug.Log($"music question is: {personalityQuestions[musicQuestionIndex]}");
    }


    public void ShowNextQuestion()
    {
        // if we asked our last question, turn ourselves off
        if(endingQuestionCounter >= endingQuestions.Length) {
            return;
        }

        // make sure buttons are active
        if(answer1Text.gameObject.activeInHierarchy == false) {
            answer1Text.transform.parent.gameObject.SetActive(true);
            answer2Text.transform.parent.gameObject.SetActive(true);
        }

        // handle story questions (we alternate here)
        if(currentQuestionType == QuestionType.Story) {
            QuestionType questionType = justAskedStoryQuestion? QuestionType.Complex : QuestionType.Story;

            currentQuestion = GetRandomQuestion(questionType);
            questionText.text = currentQuestion.question;
            answer1Text.text  = currentQuestion.answer1;
            answer2Text.text  = currentQuestion.answer2;     

            justAskedStoryQuestion = !justAskedStoryQuestion;
        }
        // handle ending questions
        else if(currentQuestionType == QuestionType.Ending) {
            Debug.Log($"at ending, with counter: {endingQuestionCounter}");
            currentQuestion = endingQuestions[endingQuestionCounter++];
            questionText.text = currentQuestion.question;
            answer1Text.text  = currentQuestion.answer1;
            answer2Text.text  = currentQuestion.answer2;

            // if we asked our last question, turn ourselves off
            if(endingQuestionCounter >= endingQuestions.Length) {
                this.enabled = false;
            }
        }
        // if we haven't shown music question yet, show it
        else if(currentQuestionType == QuestionType.Personality && !hasShownMusicQuestion) {
            Debug.Log($"showing music question! question count: {questionCounter}");
            currentQuestion = personalityQuestions[musicQuestionIndex];
            questionText.text = currentQuestion.question;
            answer1Text.text  = currentQuestion.answer1;
            answer2Text.text  = currentQuestion.answer2;

            MarkQuestionAsShown(currentQuestion.question, QuestionType.Personality);
            hasShownMusicQuestion = true;
        }
        // otherwise show random question
        else {
            currentQuestion = GetRandomQuestion();
            questionText.text = currentQuestion.question;
            answer1Text.text  = currentQuestion.answer1;
            answer2Text.text  = currentQuestion.answer2;
        }

        // check if we need to go to the next set of questions
        if(MoveOnToNextType() && currentQuestionType != QuestionType.Ending) {
            GoToNextQuestionType();
        }

        questionCounter++;
    }


    public void ReaskToSayGoodbye()
    {
        Invoke("AskGoodbye", 5);
    }


    void AskGoodbye()
    {
        // make sure buttons are active
        if(answer1Text.gameObject.activeInHierarchy == false) {
            answer1Text.transform.parent.gameObject.SetActive(true);
            answer2Text.transform.parent.gameObject.SetActive(true);
        }

        currentQuestion = endingQuestions[1];
        questionText.text = currentQuestion.question;
        answer1Text.text  = currentQuestion.answer1;
        answer2Text.text  = currentQuestion.answer2;

        endingQuestionCounter = 2;
    }


    public void SubmitAnswer(string answer)
    {
        // did they choose answer 1?
        if(answer == "answer1") {
            currentQuestion.answer1Event.Invoke();
            CharacterFlagsTracker.instance.RegisterAttribute(currentQuestion.answer1Attribute);
        }
        // did they choose answer 2?
        else if(answer == "answer2") {
            currentQuestion.answer2Event.Invoke();
            CharacterFlagsTracker.instance.RegisterAttribute(currentQuestion.answer2Attribute);
        }
        // something went wrong...
        else {
            Debug.LogError($"unknown message passed to SubmitAnswer: {answer}");
        }

        // do we need to track changes?
        if(answer == "answer1" && currentQuestion.answer1TrackChange) {
            makeLotsScript.TrackChange();
        }
        else if(answer == "answer2" && currentQuestion.answer2TrackChange) {
            makeLotsScript.TrackChange();
        }

        // hide UI
        questionText.text = "";
        answer1Text.transform.parent.gameObject.SetActive(false);
        answer2Text.transform.parent.gameObject.SetActive(false);

        // show next question
        if(!AskedToWaitToSayGoodbye(answer))
            Invoke("ShowNextQuestion", waitTimeBetweenQuestions);
    }


    QuestionAndAnswer GetRandomQuestion()
    {
        return GetRandomQuestion(currentQuestionType);
    }


    QuestionAndAnswer GetRandomQuestion(QuestionType questionType)
    {
        // get appropriate set of questions
        QuestionAndAnswer[] questions;
        switch(questionType) {
            case QuestionType.Appearance:
                questions = appearanceQuestions;
                break;
            case QuestionType.Personality:
                questions = personalityQuestions;
                break;
            case QuestionType.Environment:
                questions = environmentQuestions;
                break;
            case QuestionType.Story:
                questions = storyQuestions;
                break;
            case QuestionType.Complex:
                questions = complexQuestions;
                break;
            case QuestionType.Ending:
                questions = endingQuestions;
                break;
            default:
                Debug.LogError($"unknown question type {questionType.ToString()}");
                questions = new QuestionAndAnswer[0];
                break;
        }

        // get questions that haven't been shown already
        questions = questions.Where(q => q.hasBeenShown == false).ToArray();

        // if no more questions...just throw an error for now
        if(questions.Length == 0) {
            Debug.LogError($"no more questions for category {questionType.ToString()}!");
            if(questionType == QuestionType.Ending) {
                return new QuestionAndAnswer();
            }
            else {
                GoToNextQuestionType();
                return GetRandomQuestion();
            }            
        }

        // get a random question
        QuestionAndAnswer randomQuestion = questions[Random.Range(0, questions.Length)];

        // mark it as shown
        MarkQuestionAsShown(randomQuestion.question, questionType);

        // and return
        return randomQuestion;
    }


    void MarkQuestionAsShown(string question, QuestionType questionType)
    {
        switch(questionType) {
            case QuestionType.Appearance:
                foreach(var q in appearanceQuestions) {
                    if(q.question == question) {
                        q.hasBeenShown = true;
                        return;
                    }
                }
                break;
            case QuestionType.Personality:
                foreach(var q in personalityQuestions) {
                    if(q.question == question) {
                        q.hasBeenShown = true;
                        return;
                    }
                }
                break;
            case QuestionType.Environment:
                foreach(var q in environmentQuestions) {
                    if(q.question == question) {
                        q.hasBeenShown = true;
                        return;
                    }
                }
                break;
            case QuestionType.Story:
                foreach(var q in storyQuestions) {
                    if(q.question == question) {
                        q.hasBeenShown = true;
                        return;
                    }
                }
                break;
            case QuestionType.Complex:
                foreach(var q in complexQuestions) {
                    if(q.question == question) {
                        q.hasBeenShown = true;
                        return;
                    }
                }
                break;
            default:
                Debug.LogError($"unknown question type {currentQuestionType.ToString()}");
                break;
        }
    }


    void GoToNextQuestionType()
    {
        questionCounter = 0;
        currentQuestionType++;
    }


    bool AskedToWaitToSayGoodbye(string answer)
    {
        return endingQuestionCounter == 2 && answer == "answer2";
    }


    void InitQuestions()
    {
        // mark all appearance as not shown
        foreach(var q in appearanceQuestions) {
            q.hasBeenShown = false;
        }
        // mark all personality as not shown
        foreach(var q in personalityQuestions) {
            q.hasBeenShown = false;
        }
        // mark all environment as not shown
        foreach(var q in environmentQuestions) {
            q.hasBeenShown = false;
        }
        // mark all story as not shown
        foreach(var q in storyQuestions) {
            q.hasBeenShown = false;
        }

        
        // init move on question
        moveOnQuestion = new QuestionAndAnswer();
        moveOnQuestion.question = MOVE_ON_QUESTION_TEXT;
        moveOnQuestion.answer1 = "Yes";
        moveOnQuestion.answer2 = "Not yet";
        moveOnQuestion.answer1Event = new UnityEvent();
        moveOnQuestion.answer1Event.AddListener(GoToNextQuestionType);
        moveOnQuestion.answer2Event = new UnityEvent();
    }


    bool MoveOnToNextType()
    {
        int targetQuestionCount = 0;
        switch(currentQuestionType) {
            case QuestionType.Appearance:
                targetQuestionCount = numAppearanceQuestions;
                break;
            case QuestionType.Personality:
                targetQuestionCount = numPersonalityQuestions;
                break;
            case QuestionType.Environment:
                targetQuestionCount = numEnvironmentQuestions;
                break;
            case QuestionType.Story:
                targetQuestionCount = numStoryAndComplexQuestions;
                break;
            case QuestionType.Ending:
                targetQuestionCount = 10000;
                break;
            default:
                targetQuestionCount = 10000;
                Debug.LogError($"not sure if we should move on because the type is unaccounted: {currentQuestionType.ToString()}");
                break;
        }

        Debug.Log($"move on to next ({questionCounter} >= {targetQuestionCount})? {questionCounter >= targetQuestionCount}");
        return questionCounter + 1 >= targetQuestionCount;
    }
}
