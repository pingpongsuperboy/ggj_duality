using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class YawnController : MonoBehaviour
{
    public TMP_Text yawnText;
    public float waitBetweenYawns;
    public float yawningTime;
    public float postYawnTime;
    
    public bool isYawning;
    float waitTimer;
    float yawnTimer;
    float postYawnTimer;

    const string yawnTextStr = "yaawwnnn";
    float charactersPerSecond;


    void Start()
    {
        // isYawning = false;
        waitTimer = 0;
        yawnTimer= 0;
        postYawnTimer = 0;

        yawnText.text = "";
    }


    void Update()
    {
        if(isYawning) {
            if(waitTimer < waitBetweenYawns) {
                // keep waiting
            }
            else if(yawnTimer < yawningTime) {
                // yawn!!
                Yawn();
                yawnTimer += Time.deltaTime;
            }
            else if(postYawnTimer < postYawnTime) {
                // increment timer
                postYawnTimer += Time.deltaTime;
            }
            else {
                // reset timers
                waitTimer = 0;
                yawnTimer = 0;
                postYawnTimer = 0;

                // wipe out text
                yawnText.text = "";
            }
        }
        else {
            // do nothing, for now...
        }

        waitTimer += Time.deltaTime;
    }


    void Yawn()
    {
        int desiredCharsShowing = Mathf.RoundToInt((float)yawnTextStr.Length * (yawnTimer / yawningTime));
        yawnText.text = yawnTextStr.Substring(0,desiredCharsShowing);
    }


    public void StartYawning(float waitTimeBetweenYawns)
    {
        isYawning = true;
        waitBetweenYawns = waitTimeBetweenYawns;
    }
}
