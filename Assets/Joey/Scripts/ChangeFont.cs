using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeFont : MonoBehaviour
{
    public Text[] textsInScene;
    public Font newFont;


    public void ApplyChange()
    {
        foreach(var text in textsInScene) {
            text.font = newFont;
        }
    }
}
