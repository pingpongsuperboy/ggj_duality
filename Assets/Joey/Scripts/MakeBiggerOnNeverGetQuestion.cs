using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeBiggerOnNeverGetQuestion : MonoBehaviour
{
    public Text questionText;
    public float newHeight;

    const float newPosY = -178.8f;

    const string neverGetQuestion = "Are they still looking for something they will never get?";
    Vector2 startingDimensions;
    Vector2 customDimensions;
    Vector3 startingPosition;
    Vector3 customPosition;
    RectTransform rect;

    void Start()
    {
        rect = GetComponent<RectTransform>();
        startingDimensions = rect.sizeDelta;
        startingPosition = rect.localPosition;
        customDimensions = new Vector2(startingDimensions.x, newHeight);
        customPosition = new Vector3(startingPosition.x, newPosY, startingPosition.z);
    }


    void Update()
    {
        if(IsTwoLines()) {
            rect.sizeDelta = customDimensions;
            rect.localPosition = customPosition;
        }
        else {
            rect.sizeDelta = startingDimensions;
            rect.localPosition = startingPosition;
        }
    }


    bool IsTwoLines()
    {
        Canvas.ForceUpdateCanvases();
        return questionText.cachedTextGenerator.lines.Count > 1;
    }
}
