using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterFlagsTracker : MonoBehaviour
{
    public static CharacterFlagsTracker instance;

    public enum Attribute {
        NA, Cube, Sphere, TwoEyes, SixEyes, BackHands, BellyHands,
        Penguin, Guitar, Lots, Turquoise, BigFamily, LongArms, ShortArms,
        HasMouth, HasShield, HasStaff, LongNose, ShortNose, IsStatic, NotStatic,
        YawnsLots, YawnsNever, UsesToilet, NoToilet, IsBig, NotBig, ManyLegs, FewLegs,
        ElephantEars, GiraffeEars, HasFriends, LikesToDance
    }
    

    HashSet<Attribute> attributes;


    void Awake()
    {
        if(instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        attributes = new HashSet<Attribute>();
    }


    public void RegisterAttribute(Attribute attribute)
    {
        attributes.Add(attribute);
    }


    public bool HasAttribute(Attribute attribute)
    {
        return attributes.Contains(attribute);
    }
}
