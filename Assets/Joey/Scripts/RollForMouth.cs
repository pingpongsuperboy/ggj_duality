using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RollForMouth : MonoBehaviour
{
    // public GameObject mouth;
    public TMP_Text mouth;
    public TMP_Text penguinMouth;

    SmartPenguinActivator smartPenguin;
    bool isSmiling;

    const string frown = "(";


    void Start()
    {
        smartPenguin = GetComponent<SmartPenguinActivator>();
        isSmiling = false;
    }


    public void Roll(int percentToSucceed)
    {
        int roll = Random.Range(0, 100);
        if(roll < percentToSucceed) {
            isSmiling = true;
            smartPenguin.ActivateAnswer1();
        }
    }


    public void Frown()
    {
        if(isSmiling) {
            mouth.text = frown;
            penguinMouth.text = frown;
        }
    }
}
