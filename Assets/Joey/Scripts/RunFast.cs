using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunFast : MonoBehaviour
{
    public Animator twoLegsAnimation;
    public Animator sixLegsAnimation;
    public Animator twoLegsPenguinAnimation;
    public Animator sixLegsPenguinAnimation;

    public SmartPenguinActivator legsActivator;


    public void StartAnimation()
    {
        // if they don't have legs, let's just add legs
        if(!HasTwoLegs() && !HasSixLegs()) {
            legsActivator.ActivateAnswer1();
        }

        if(IsPenguin()) {
            if(HasTwoLegs()) {
                twoLegsPenguinAnimation.enabled = true;
            }
            else {
                twoLegsPenguinAnimation.enabled = true;
                sixLegsPenguinAnimation.enabled = true;
            }
        }
        else {
            if(HasTwoLegs()) {
                twoLegsAnimation.enabled = true;
            }
            else {
                twoLegsAnimation.enabled = true;
                sixLegsAnimation.enabled = true;
            }
        }
    }


    bool IsPenguin()
    {
        return CharacterFlagsTracker.instance.HasAttribute(CharacterFlagsTracker.Attribute.Penguin);
    }


    bool HasTwoLegs()
    {
        return CharacterFlagsTracker.instance.HasAttribute(CharacterFlagsTracker.Attribute.FewLegs);
    }


    bool HasSixLegs()
    {
        return CharacterFlagsTracker.instance.HasAttribute(CharacterFlagsTracker.Attribute.ManyLegs);
    }
}
