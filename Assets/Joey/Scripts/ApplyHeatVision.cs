using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyHeatVision : MonoBehaviour
{
    public GameObject twoEyes;
    public GameObject sixEyes;
    public GameObject twoEyesPenguin;
    public GameObject sixEyesPenguin;


    public void Apply()
    {
        if(IsPenguin()) {
            if(HasSixEyes()) {
                sixEyesPenguin.SetActive(true);
            }
            // if they either have two eyes, or no eyes, we set two eyes to active
            else {
                twoEyesPenguin.SetActive(true);
            }
        }
        else {
            if(HasSixEyes()) {
                sixEyes.SetActive(true);
            }
            // if they either have two eyes, or no eyes, we set two eyes to active
            else {
                twoEyes.SetActive(true);
            }
        }
    }


    bool IsPenguin()
    {
        return CharacterFlagsTracker.instance.HasAttribute(CharacterFlagsTracker.Attribute.Penguin);
    }


    bool HasTwoEyes()
    {
        return CharacterFlagsTracker.instance.HasAttribute(CharacterFlagsTracker.Attribute.TwoEyes);
    }


    bool HasSixEyes()
    {
        return CharacterFlagsTracker.instance.HasAttribute(CharacterFlagsTracker.Attribute.SixEyes);
    }
}
