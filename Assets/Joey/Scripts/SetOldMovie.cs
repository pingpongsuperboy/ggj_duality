using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class SetOldMovie : MonoBehaviour
{
    public PostProcessProfile oldMovie;
    

    PostProcessVolume volume;

    void Start()
    {
        volume = Camera.main.GetComponent<PostProcessVolume>();
    }


    public void Apply()
    {
        volume.profile = oldMovie;
    }
}
