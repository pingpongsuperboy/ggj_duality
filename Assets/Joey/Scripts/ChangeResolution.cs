using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeResolution : MonoBehaviour
{
    int highDefX = 1920;
    int highDefY = 1080;

    int lowDefX = 1024;
    int lowDefY = 576;


    public void SetHighDef()
    {
        Debug.Log("setting to high def!");
        Screen.SetResolution(highDefX, highDefY, FullScreenMode.ExclusiveFullScreen);
    }


    public void SetLowDef()
    {
        Debug.Log("setting to low def!");
        Screen.SetResolution(lowDefX, lowDefY, FullScreenMode.ExclusiveFullScreen);
    }
}
