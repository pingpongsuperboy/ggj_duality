using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSkybox : MonoBehaviour
{
    public Material daytime;
    public Material nighttime;


    public void SetDaytime()
    {
        Camera.main.clearFlags = CameraClearFlags.Skybox;
        RenderSettings.skybox = daytime;
    }


    public void SetNighttime()
    {
        Camera.main.clearFlags = CameraClearFlags.Skybox;
        RenderSettings.skybox = nighttime;
    }
}
