using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotifySmartPenguins : MonoBehaviour
{
    SmartPenguinActivator[] smartPenguins;


    void Start()
    {
        // find all smart penguins in scene
        smartPenguins = GameObject.FindObjectsOfType<SmartPenguinActivator>();

        Debug.Log($"found {smartPenguins.Length} smart penguins!");
    }


    public void SendNotification()
    {
        foreach(var sp in smartPenguins) {
            sp.OnBecomePenguin();
        }
    }
}
