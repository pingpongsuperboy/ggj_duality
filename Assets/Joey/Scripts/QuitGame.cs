using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    public void Quit()
    {
        StartCoroutine("FadeOutAudio");
    }


    IEnumerator FadeOutAudio()
    {
        // we'll need to track the longest fade
        float longestFadeTime = 0;

        // tell all the audio to fade itself out
        FadeAudioInOut[] fadeScripts = GameObject.FindObjectsOfType<FadeAudioInOut>();
        foreach(var fader in fadeScripts) {
            fader.BeginFadeOut();
            longestFadeTime = Mathf.Max(longestFadeTime, fader.fadeOutTime);
        }

        // wait until all scripts have faded out, plus a little extra
        float timer = 0;
        float padding = 3.0f;
        while(timer < longestFadeTime + padding) {
            timer += Time.deltaTime;
            yield return null;
        }

        // quit game
        Application.Quit();
    }
}
