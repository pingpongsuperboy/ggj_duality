using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dance : MonoBehaviour
{

    public float percent;
    float mod = 1;
    public Vector3 start;
    public Vector3 destination;
    public Vector3 from;
    public Vector3 to;
    public bool danceOnAwake;

    bool isDancing;

    void Start()
    {
        percent = 0;
        isDancing = danceOnAwake;
        start = new Vector3(transform.position.x, transform.position.y , transform.position.z);
        destination = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);

    }


    void Update()
    {
        if(!isDancing)
            return;

        percent += Time.deltaTime * mod;

        if (percent <= 0 || percent > 0.5) // make this animation loop
        {
            mod *= -1;
            percent += Time.deltaTime * mod;
        }

        // use Lerp to make this item move up and down
        transform.position = Vector3.Lerp(start, destination, percent);
        transform.eulerAngles = Vector3.Lerp(from, to, percent);


    }


    public void StartDancing()
    {
        isDancing = true;
    }
}
